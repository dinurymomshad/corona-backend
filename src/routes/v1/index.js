const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const responseRoute = require('./response.route');

const router = express.Router();

router.use('/auth', authRoute);
router.use('/users', userRoute);
router.use('/response', responseRoute);

module.exports = router;
