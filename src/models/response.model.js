const mongoose = require('mongoose');
const validator = require('validator');
const { pick } = require('lodash');

const responseSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    phone: {
      type: String,
      required: false,
      unique: true,
      trim: true,
      lowercase: true,
    },
    mobile: {
      type: String,
      required: false,
      unique: true,
      trim: true,
      lowercase: true,
    },
    email: {
      type: String,
      required: false,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Invalid email');
        }
      },
    },
  },
  {
    timestamps: true,
    toObject: { getters: true },
    toJSON: { getters: true },
  },
);

responseSchema.methods.transform = function() {
  const response = this;
  return pick(response.toJSON(), ['id', 'name']);
};

const Response = mongoose.model('Response', responseSchema);

module.exports = Response;
