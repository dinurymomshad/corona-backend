const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { responseService } = require('../services');

const createResponse = catchAsync(async (req, res) => {
  const response = await responseService.createResponse(req.body);
  res.status(httpStatus.CREATED).send(response.transform());
});

const getResponses = catchAsync(async (req, res) => {
  const responses = await responseService.getResponses(req.query);
  const result = responses.map(response => response.transform());
  res.send(result);
});

const getResponse = catchAsync(async (req, res) => {
  const response = await responseService.getResponseById(req.params.id);
  res.send(response.transform());
});

const updateResponse = catchAsync(async (req, res) => {
  const response = await responseService.updateResponse(req.params.id, req.body);
  res.send(response.transform());
});

module.exports = {
  createResponse,
  getResponses,
  getResponse,
  updateResponse,
};
