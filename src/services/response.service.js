const httpStatus = require('http-status');
const { pick } = require('lodash');
const AppError = require('../utils/AppError');
const { Response } = require('../models');
const { getQueryOptions } = require('../utils/service.util');

const createResponse = async responseBody => {
  return Response.create(responseBody);
};

const getResponses = async query => {
  const filter = pick(query, ['name']);
  const options = getQueryOptions(query);
  const responses = await Response.find(filter, null, options);
  return responses;
};

const getResponseById = async id => {
  const response = await Response.findById(id);
  if (!response) {
    throw new AppError(httpStatus.NOT_FOUND, 'Response not found');
  }
  return response;
};


const updateResponse = async (id, updateBody) => {
  const response = await getResponseById(id);
  Object.assign(response, updateBody);
  await response.save();
  return response;
};

module.exports = {
  createResponse,
  getResponses,
  getResponseById,
  updateResponse,
};
